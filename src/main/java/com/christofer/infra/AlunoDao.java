package com.christofer.infra;

import java.util.List;

import com.christofer.dominio.Aluno;

public interface AlunoDao {

	public abstract List<Aluno> obterAlunosSemOrientacao();
	
	public abstract Aluno obterPorId(Long id);
	
}
