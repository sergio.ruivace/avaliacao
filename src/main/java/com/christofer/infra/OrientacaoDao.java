package com.christofer.infra;

import java.util.List;

import com.christofer.dominio.Orientacao;

public interface OrientacaoDao {

	public abstract List<Orientacao> obterTodos();
	
	public abstract Orientacao obterPorId(Long id);
}
