package com.christofer.infra;

import java.util.List;

import com.christofer.dominio.ProducaoAcademica;

public interface ProducaoAcademicaDao {

	public List<ProducaoAcademica> obterTodos();
	
	public boolean incluir(ProducaoAcademica p);
	
	public boolean alterar(ProducaoAcademica p);
	
	public ProducaoAcademica obterPorId(Long id);
	
}
