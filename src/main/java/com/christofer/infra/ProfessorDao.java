package com.christofer.infra;

import java.util.List;

import com.christofer.dominio.Professor;

public interface ProfessorDao {

	public abstract List<Professor> obterTodos();
	
	public abstract Professor obterPorId(Long id); 
}
